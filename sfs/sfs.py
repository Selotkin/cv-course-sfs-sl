import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from utils import estimate_albedo_illumination
import cv2 as cv

if __name__ == '__main__':

    file = 'imgs/face.png'
    E = cv.imread(file)
    E = cv.cvtColor(E, cv.COLOR_BGR2GRAY)
    [M, N] = np.shape(E)
    M = M
    N = N
    [albedo, I, slant, tilt] = estimate_albedo_illumination(E)


    p = np.zeros((M, N))
    q = np.zeros((M, N))

    z = np.zeros((M, N))

    z_x = np.zeros((M, N))
    z_y = np.zeros((M, N))

    [x,y] = np.meshgrid(np.linspace(1, N, N), np.linspace(1, M, M))

    max_iter = 50 

    ix = np.cos(tilt) * np.tan(slant)
    iy = np.sin(tilt) * np.tan(slant)
    eps = 1e-5
    for i in range(max_iter):

        R = (np.cos(slant) + p * np.cos(tilt) * np.sin(slant) + q * np.sin(tilt) * np.sin(slant)) / np.sqrt(1 + p**2 + q**2)
        index = np.where(R < 0)
        R[index[0], index[1]] = 1

        f = E - R

        df_dz = (p + q) * (ix * p + iy * q + 1) / (np.sqrt((1 + p**2 + q**2)**3) * (np.sqrt(1 + ix**2 + iy**2))) - (ix + iy) / (np.sqrt(1 + p**2 + q**2) * np.sqrt(1 + ix**2 + iy**2))
        
        z = z - f / (df_dz + eps)

        z_x[1:M,:] = z[0:M-1,:]
        z_y[:,1:N] = z[:, 0:N-1]

        p = z - z_x
        q = z - z_y

    fig = plt.figure()
    ax = Axes3D(fig)
    ax.plot_surface(x, y, np.abs(z), rcount = 100, ccount = 100, cmap = 'ocean')
    plt.show()
