import numpy as np

def lpf(img, freq):
    rows, cols = img.shape
    crow, ccol = int(rows/2), int(cols/2)
    mask = np.zeros((rows, cols), np.uint8)
    mask[crow-freq:crow+freq, ccol-freq:ccol+freq] = 1
    dft = np.fft.fft2(img)
    dft_shift = np.fft.fftshift(dft)
    fshift = dft_shift*mask
    f_ishift = np.fft.ifftshift(fshift)
    img_back = np.fft.ifft2(f_ishift)
    img_back = np.absolute(img_back)
    img_back = (img_back - np.min(img_back))/(np.max(img_back) - np.min(img_back))
    return img_back

def q_lpf(ref, img):
    rows, cols = img.shape
    crow, ccol = int(rows/2), int(cols/2)
    mask = np.zeros((rows, cols), np.uint8)
    dft_img = np.fft.fft2(img)
    dft_shift_img = np.fft.fftshift(dft_img)

    dft_ref = np.fft.fft2(ref)
    dft_shift_ref = np.fft.fftshift(dft_ref)

    fshift = dft_shift_img - dft_shift_ref
    f_ishift = np.fft.ifftshift(fshift)
    img_back = np.fft.ifft2(f_ishift)
    img_back = np.absolute(img_back)
    img_back = (img_back - np.min(img_back))/(np.max(img_back) - np.min(img_back))
    return img_back

def wrap(x):
    while np.abs(x) > np.pi:
        x = x - np.sign(x) * 2 * np.pi
    return x

vwrap = np.vectorize(wrap)
