import open3d as o3d
import compute_pc
import feature_matching

if __name__ == "__main__":

	data = compute_pc.compute()

	pc, mesh = feature_matching.get_pcmesh(data)

	o3d.visualization.draw_geometries([pc])  
	o3d.visualization.draw_geometries([mesh])  