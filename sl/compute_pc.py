import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import cv2
import utils

def read_refs(img_num, f1, f2):
    lf_refs = []
    hf_refs = []
    for i in range(img_num):
        filename = f'refs/{f1}_ref{i}.png'
        img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
        gray = np.asarray(img, dtype="float32")
        ref = gray - utils.lpf(gray, 40)
        lf_refs.append(ref)
        filename = f'refs/{f2}_ref{i}.png'
        img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
        gray = np.asarray(img, dtype="float32")
        ref = gray - utils.lpf(gray, 40)
        hf_refs.append(ref)
    return lf_refs, hf_refs

def get_phase(object_type, img_num, f, refs, angle):

    images = []
    for i in range(img_num):
        filename = f'bunny/{angle}_{f}_phase{i}.png'
        img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
        gray = np.asarray(img, dtype="float32")
        images.append(gray)

    images_x = []
    for i in range(len(images)):
        images_x.append(images[i] - utils.q_lpf(refs[i], images[i]))

    a = sum(images_x) / img_num
    filter_img = []
    for img in images_x:
        img = img - a
        filter_img.append(img)

    alpha = np.pi/(img_num/2)

    a = np.zeros(refs[0].shape)
    b = np.zeros(refs[0].shape)

    for i in range(img_num):
        a -= refs[i] * np.sin(i*alpha)
        b += refs[i] * np.cos(i*alpha)

    ref_phase = np.arctan2(a, b)

    a = np.zeros(refs[0].shape)
    b = np.zeros(refs[0].shape)

    for i in range(img_num):
        a -= filter_img[i] * np.sin(i*alpha)
        b += filter_img[i] * np.cos(i*alpha)

    test_phase = np.arctan2(a, b)

    phase = test_phase - ref_phase
    return phase

def temporal_unwrap(phase1, phase2, scale, real_scale):

    kernel = np.ones((5,5),np.float32)/25
    phase1 = cv2.filter2D(phase1,-1,kernel)

    k1 = np.around((scale*phase1 - phase2)*0.5/np.pi)
    phi2 = phase2 + 2*k1*np.pi
    phi2 = phi2*real_scale

    return phi2

def compute():

    object_type = 'bunny' 
    img_num = 4 #number of images
    f1 = 'lf'
    f2 = 'hf'
    scale = 37 #scale for each high-frequency pattern
    real_scale = 0.2432 #scale to convert phase to real value different for each frequncy
    
    lf_refs, hf_refs = read_refs(img_num, f1, f2)

    angles = [0, 90, 180, 270, 45, 135, 225, 315]
    
    depths = []
    for i in tqdm(range(8)):
        phase1 = get_phase(object_type, img_num, f1, lf_refs, angles[i])
        phase1 = utils.vwrap(phase1)

        phase2 = get_phase(object_type, img_num, f2, hf_refs, angles[i])
        depth = temporal_unwrap(phase1, phase2, scale, real_scale)
        np.save(f"bunny_{i}", depth)
        depths.append(depth)
    
    x = np.arange(-5, 5, 10/depth.shape[1])
    y = np.arange(-5, 5, 10/depth.shape[0])
    x, y = np.meshgrid(x, y)
    
    datas = []

    for k in range(8):

        img = depths[k]
        idxs = np.abs(img) > 0.02
        x_temp = x[idxs]
        y_temp = y[idxs]
        img_temp = img[idxs]

        data = np.asarray([x_temp, y_temp, img_temp]).T
        
        data[:, 2] -= 3.5
        rot_mat = np.array([[1, 0, 0], [0, 0.5, -0.866], [0, 0.866, 0.5]])
        data = np.dot(data, rot_mat)
        
        if(k==1):
            rot_mat = np.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]])
            data = np.dot(data, rot_mat)
        if(k==2):
            rot_mat = np.array([[-1, 0, 0], [0, -1, 0], [0, 0, 1]])
            data = np.dot(data, rot_mat)
        if(k==3):
            rot_mat = np.array([[0, 1, 0], [-1, 0, 0], [0, 0, 1]])
            data = np.dot(data, rot_mat)
        if(k==4):
            rot_mat = np.array([[0.866, -0.5, 0], [0.5, 0.866, 0], [0, 0, 1]])
            data = np.dot(data, rot_mat)
        if(k==5):
            rot_mat = np.array([[-0.866, -0.5, 0], [0.5, -0.866, 0], [0, 0, 1]])
            data = np.dot(data, rot_mat)
        if(k==6):
            rot_mat = np.array([[-0.866, 0.5, 0], [-0.5, -0.866, 0], [0, 0, 1]])
            data = np.dot(data, rot_mat)
        if(k==7):
            rot_mat = np.array([[0.866, 0.5, 0], [-0.5, 0.866, 0], [0, 0, 1]])
            data = np.dot(data, rot_mat)
        data[:, 2] += 3.5
        
        datas.append(data)
    	
    return np.asarray(datas)
