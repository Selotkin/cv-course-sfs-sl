import open3d as o3d
import numpy as np
import copy
import matplotlib.pyplot as plt


def draw_registration_result(source, target, transformation):
    source_temp = copy.deepcopy(source)
    target_temp = copy.deepcopy(target)
    source_temp.paint_uniform_color([1, 0.706, 0])
    target_temp.paint_uniform_color([0, 0.651, 0.929])
    source_temp.transform(transformation)
    o3d.visualization.draw_geometries([source_temp, target_temp])

def preprocess_point_cloud(pcd, voxel_size, k):

    pcd_down = pcd.voxel_down_sample(voxel_size)

    pcd_down.paint_uniform_color([0.5, 0.5, 0.5])

    radius_normal = voxel_size * 2
    pcd_down.estimate_normals(
        o3d.geometry.KDTreeSearchParamHybrid(radius=voxel_size * 4, max_nn=100), fast_normal_computation=False)

    cam_pos = np.array([0, 0, 100])
    
    rot_mat = np.array([[1, 0, 0], [0, 0.5, -0.866], [0, 0.866, 0.5]])
    cam_pos = np.dot(cam_pos, rot_mat)
    if(k==1):
        rot_mat = np.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]])
        cam_pos = np.dot(cam_pos, rot_mat)
    if(k==2):
        rot_mat = np.array([[-1, 0, 0], [0, -1, 0], [0, 0, 1]])
        cam_pos = np.dot(cam_pos, rot_mat)
    if(k==3):
        rot_mat = np.array([[0, 1, 0], [-1, 0, 0], [0, 0, 1]])
        cam_pos = np.dot(cam_pos, rot_mat)
    if(k==4):
        rot_mat = np.array([[0.866, -0.5, 0], [0.5, 0.866, 0], [0, 0, 1]])
        cam_pos = np.dot(cam_pos, rot_mat)
    if(k==5):
        rot_mat = np.array([[-0.866, -0.5, 0], [0.5, -0.866, 0], [0, 0, 1]])
        cam_pos = np.dot(cam_pos, rot_mat)
    if(k==6):
        rot_mat = np.array([[-0.866, 0.5, 0], [-0.5, -0.866, 0], [0, 0, 1]])
        cam_pos = np.dot(cam_pos, rot_mat)
    if(k==7):
        rot_mat = np.array([[0.866, 0.5, 0], [-0.5, 0.866, 0], [0, 0, 1]])
        cam_pos = np.dot(cam_pos, rot_mat)
    
    pcd_down.orient_normals_towards_camera_location(cam_pos)

    cl, ind = pcd_down.remove_statistical_outlier(nb_neighbors=11,
                                            std_ratio=2.5)
    pcd_down = pcd_down.select_down_sample(ind)
    cl, ind = pcd_down.remove_radius_outlier(nb_points=15, radius=0.047)
    pcd_down = pcd_down.select_down_sample(ind)
    cl, ind = pcd_down.remove_radius_outlier(nb_points=15, radius=0.06)
    pcd_down = pcd_down.select_down_sample(ind)
    cl, ind = pcd_down.remove_radius_outlier(nb_points=15, radius=0.07)
    pcd_down = pcd_down.select_down_sample(ind)

    radius_feature = voxel_size * 5
    pcd_fpfh = o3d.registration.compute_fpfh_feature(
        pcd_down,
        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))
    
    return pcd_down, pcd_fpfh

def refine_registration(source, target, source_fpfh, target_fpfh, voxel_size):
    distance_threshold = voxel_size * 0.4
    result = o3d.registration.registration_icp(
        source, target, distance_threshold, np.identity(4),
        o3d.registration.TransformationEstimationPointToPlane())
    return result


def prepare_data(voxel_size, points, i):

    source = o3d.geometry.PointCloud()

    source.points = o3d.utility.Vector3dVector(points)
    source_down, source_fpfh = preprocess_point_cloud(source, voxel_size, i)

    return source_down, source_fpfh


def get_pcmesh(data):
    
    voxel_size = 0.02
    order = [4,1,5,2,6,3,7]
    
    target_down, target_fpfh = prepare_data(voxel_size, data[0], 0)
    result = target_down

    for i in np.arange(8):
        source_down, source_fpfh = prepare_data(voxel_size, data[i], i)

        result_icp = refine_registration(source_down, target_down, source_fpfh, 
                                         target_fpfh, voxel_size)
        source_down.transform(result_icp.transformation)
        draw_registration_result(source_down, target_down, np.identity(4))

        result += source_down
        target_down = source_down
        target_fpfh = source_fpfh
    
      
    mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(result, depth = 10)
    mesh.compute_triangle_normals()
    mesh.compute_vertex_normals()

    return result, mesh
